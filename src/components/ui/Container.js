import styled from "styled-components/native";
import Constants from "expo-constants";

export default styled.View`
  flex: 1;
  padding-top: ${Constants.statusBarHeight}px;
`;
