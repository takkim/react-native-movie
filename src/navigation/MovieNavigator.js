import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import MovieList from "../screens/MovieList";
import MovieDetail from "../screens/MovieDetail";

const Stack = createStackNavigator();

const MovieNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="MovieList">
      <Stack.Screen
        name="MovieList"
        component={MovieList}
        options={{ headerShown: false }}
      />
      <Stack.Screen name="MovieDetail" component={MovieDetail} />
    </Stack.Navigator>
  );
};

export default MovieNavigator;
