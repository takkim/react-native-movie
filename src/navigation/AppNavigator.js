import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import MovieNavigator from "./MovieNavigator";
import MovieSearch from "../screens/MovieSearch";

const Drawer = createDrawerNavigator();

const AppNavigator = () => {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen
        name="MovieNavigator"
        component={MovieNavigator}
        options={{
          drawerLabel: "영화 목록",
          headerShown: false,
        }}
      />
      <Drawer.Screen name="MovieSearch" component={MovieSearch} />
    </Drawer.Navigator>
  );
};

export default AppNavigator;
