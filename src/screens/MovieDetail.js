import React, { useState, useEffect } from "react";
import Container from "../components/ui/Container";
import Text from "../components/ui/Text";
import axios from "axios";
import styled from "styled-components/native";

const Title = styled.Text`
  font-size: 24px;
  font-weight: bold;
`;

const SubTitle = styled.Text`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 10px;
`;
const MovieDetail = ({ route }) => {
  const [movie, setMovie] = useState({});
  useEffect(() => {
    const url =
      "http://www.kobis.or.kr/kobisopenapi/webservice/rest/movie/searchMovieInfo.json";
    axios
      .get(url, {
        params: {
          key: "a88ff88a2b271337aa094fb50353f261",
          movieCd: route.params.movieCd,
        },
      })
      .then((response) => {
        console.log(response.data);
        setMovie(response.data.movieInfoResult.movieInfo);
      });
  }, []);
  return (
    <Container>
      <Title> {movie.movieNm} </Title>
      <SubTitle> {movie.movieNmEn} </SubTitle>
      <Text> 개봉년도 : {movie.openDt}</Text>
      <Text> 상영시간 : {movie.showTm}분</Text>
      <Text>
        {" "}
        장르 : {movie.genres?.map((genre) => genre.genreNm).join(", ")}
      </Text>
      <Text>
        {" "}
        출연진 : {movie.actors?.map((actor) => actor.peopleNm).join(", ")}{" "}
      </Text>
    </Container>
  );
};

export default MovieDetail;
