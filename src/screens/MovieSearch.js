import React, { useState, useCallback } from "react";
import { Button } from "react-native";
import Container from "../components/ui/Container";
import styled from "styled-components/native";
import Text from "../components/ui/Text";
import axios from "axios";
const Input = styled.TextInput`
  border: 1px solid lightgray;
  width: 80%;
  height: 50px;
`;

const MovieSearch = () => {
  const [movies, setMovies] = useState([]);
  const [query, setQuery] = useState("");
  const search = useCallback(() => {
    // API로부터 영화 정보를 가져와야함!
    const url =
      "http://www.kobis.or.kr/kobisopenapi/webservice/rest/movie/searchMovieList.json";
    axios
      .get(url, {
        params: {
          key: "a88ff88a2b271337aa094fb50353f261",
          movieNm: query,
        },
      })
      .then((response) => {
        console.log(response.data);
        setMovies(response.data.movieListResult.movieList);
      });
  }, [query]);
  return (
    <Container>
      <Text>영화 검색</Text>
      <Input value={query} onChangeText={setQuery} />
      <Button title="검색" onPress={search} />
      {movies.map((movie) => {
        return <Text key={movie.movieCd}>{movie.movieNm}</Text>;
      })}
    </Container>
  );
};

export default MovieSearch;
