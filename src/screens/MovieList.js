// import
import React, { useEffect, useState } from "react";
import styled from "styled-components/native";
import axios from "axios";
import Container from "../components/ui/Container";
// styled components

const Title = styled.Text`
  font-size: 32px;
  font-weight: bold;
  text-align: center;
`;
const ListItem = styled.TouchableOpacity`
  padding: 10px;
  border-bottom-color: lightgray;
  border-bottom-width: 1px;
`;
const MovieName = styled.Text`
  font-size: 18px;
  font-weight: bold;
`;
// 함수
const MovieList = ({ navigation }) => {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    // API로부터 영화 정보를 가져와야함!
    const url =
      "https://www.kobis.or.kr/kobisopenapi/webservice/rest/boxoffice/searchWeeklyBoxOfficeList.json";
    axios
      .get(url, {
        params: {
          key: "a88ff88a2b271337aa094fb50353f261",
          targetDt: "20210821",
        },
      })
      .then((response) => {
        console.log(response.data);
        setMovies(response.data.boxOfficeResult.weeklyBoxOfficeList);
      });
  }, []);
  return (
    <Container>
      <Title>영화 목록</Title>
      {movies.map((item) => {
        return (
          <ListItem
            key={item.movieCd}
            onPress={() => {
              navigation.navigate("MovieDetail", { movieCd: item.movieCd });
            }}
          >
            <MovieName>{item.movieNm}</MovieName>
          </ListItem>
        );
      })}
    </Container>
  );
};
// export
export default MovieList;
